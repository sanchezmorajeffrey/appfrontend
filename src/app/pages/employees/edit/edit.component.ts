import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../../../services/employees.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  id: number = 0;
  formEmployeeEdit: FormGroup;
  error = false;
  errorList = [] as any;
  contador = "";
  submitted = false;
  fechaNacimiento: string = "";
  mascara = "000-000000-0000S";
  constructor(public formBuilder: FormBuilder, private router: Router, private ruta: ActivatedRoute, private employeesSvc: EmployeesService) {
    this.formEmployeeEdit = this.FormEmployeeEdit();
  }

  ngOnInit(): void {
    this.ruta.params.subscribe(param => {
      this.id = param.id;
      this.employeesSvc.getOneEmployee(this.id).subscribe((res: any) => {
        this.formEmployeeEdit.get('FirstName')?.setValue(res.data.FirstName);
        this.formEmployeeEdit.get('SecondName')?.setValue(res.data.SecondName);
        this.formEmployeeEdit.get('Surname')?.setValue(res.data.Surname);
        this.formEmployeeEdit.get('SecondSurname')?.setValue(res.data.SecondSurname);
        this.formEmployeeEdit.get('Cedula')?.setValue(res.data.Cedula);
        this.formEmployeeEdit.get('DateOfBirth')?.setValue(res.data.DateOfBirth);
        this.formEmployeeEdit.get('email')?.setValue(res.data.email);
        this.formEmployeeEdit.get('status')?.setValue(res.data.status);
        this.formEmployeeEdit.get('NumeroINSS')?.setValue(res.data.NumeroINSS);
      })
    });
  }

  private FormEmployeeEdit() {
    return this.formBuilder.group({
      FirstName: ["", Validators.required],
      SecondName: ["", Validators.required],
      Surname: ["", Validators.required],
      SecondSurname: ["", Validators.required],
      email: ["", Validators.compose([Validators.required,
      Validators.pattern(
        "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"
      ),
      ]),
      ],
      Cedula: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "^(\\d{3}-(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2}))-(\\d{4}[A-Z])$"
          ),
        ]),
      ],
      NumeroINSS: ["", Validators.required],
      DateOfBirth: ["", Validators.required],
      status: [true, Validators.required],
    });
  }

  get f() {
    return this.formEmployeeEdit.controls;
  }

  saveEmployee() {
    this.employeesSvc.editEmployees(this.formEmployeeEdit.value, this.id).subscribe((res) => {
      Swal.fire({
        icon: "success",
        title: "",
        text: res.data,
      });
    });
  }

  onKeyCedula(event: any) {
    this.contador = event.target.value;
    this.contador = this.contador.toString();
    if (this.contador.length === 16) {
      this.fechaNacimiento = this.contador;
      const dia = this.fechaNacimiento.substring(4, 6);
      const mes = this.fechaNacimiento.substring(6, 8);
      const year = this.fechaNacimiento.substring(8, 10);
      const anioEvaluar = Number(this.fechaNacimiento.substring(8, 10));
      if (anioEvaluar >= 40) {
        this.fechaNacimiento = `19${year}-${mes}-${dia}`;
      } else {
        this.fechaNacimiento = `20${year}-${mes}-${dia}`;
      }
      Date.parse(this.fechaNacimiento);
    } else {
      this.fechaNacimiento = "";
    }
  }

}
