import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule} from "ngx-mask";
import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EditComponent
  ],
  imports: [
    CommonModule,
    EditRoutingModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    ReactiveFormsModule
  ]
})
export class EditModule { }
