import { EmployeesService } from '../../../services/employees.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from "sweetalert2";
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  employee: any[] = [];
  constructor(private router: Router, private employeesSvc: EmployeesService) { }

  ngOnInit(): void {
    this.loadEmployees();
  }

  loadEmployees() {
    this.employeesSvc.getEmployees().subscribe((res) => {
      this.employee = res.data;
    })
  }

  onGoToEdit(item: any) {
    this.router.navigateByUrl(`edit/${item}`)
  }

  onGoToDelete(empId: string) {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Esta seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, bórralo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.employeesSvc.deleteEmployees(empId).subscribe((res) => {
          swalWithBootstrapButtons.fire(
            'Eliminado!',
            'Tu registro ha sido eliminado.',
            'success'
          ),
          this.loadEmployees();
        })

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Tu registro está seguro',
          'error'
        )
      }
    })
  }
}
