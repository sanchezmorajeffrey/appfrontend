import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EmployeesService } from '../../../services/employees.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  submitted = false;
  formEmployeeNew: FormGroup;
  contador = "";
  fechaNacimiento: string = "";
  mascara = "000-000000-0000S";
  error = false;
  errorList = [] as any;
  constructor(public formBuilder: FormBuilder, private employeesSvc: EmployeesService, private router: Router,) {
    this.formEmployeeNew = this.FormEmployeeNew();
  }

  ngOnInit(): void {
  }

  private FormEmployeeNew() {
    return this.formBuilder.group({
      FirstName: ["", Validators.required],
      SecondName: ["", Validators.required],
      Surname: ["", Validators.required],
      SecondSurname: ["", Validators.required],
      email: ["", Validators.compose([Validators.required,
      Validators.pattern(
        "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"
      ),
      ]),
      ],
      Cedula: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "^(\\d{3}-(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2}))-(\\d{4}[A-Z])$"
          ),
        ]),
      ],
      NumeroINSS: ["", Validators.required],
      DateOfBirth: ["", Validators.required],
      status: [true, Validators.required],
    });
  }

  saveEmployee() {
    this.submitted = true;
    this.formEmployeeNew.get('DateOfBirth')?.setValue(this.fechaNacimiento);
    if (this.formEmployeeNew.invalid) {
      return;
    }
    this.employeesSvc.saveEmployees(this.formEmployeeNew.value).subscribe((res) => {
      this.router.navigateByUrl('/');
    }, error => {
      this.errorList = [];
      this.errorList.push(...error.error.lista_errores);
      this.error = true;
    });
  }

  get f() {
    return this.formEmployeeNew.controls;
  }

  onKeyCedula(event: any) {
    this.contador = event.target.value;
    this.contador = this.contador.toString();
    if (this.contador.length === 16) {
      this.fechaNacimiento = this.contador;
      const dia = this.fechaNacimiento.substring(4, 6);
      const mes = this.fechaNacimiento.substring(6, 8);
      const year = this.fechaNacimiento.substring(8, 10);
      const anioEvaluar = Number(this.fechaNacimiento.substring(8, 10));
      if (anioEvaluar >= 40) {
        this.fechaNacimiento = `19${year}-${mes}-${dia}`;
      } else {
        this.fechaNacimiento = `20${year}-${mes}-${dia}`;
      }
      Date.parse(this.fechaNacimiento);
    } else {
      this.fechaNacimiento = "";
    }
  }
}
