import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  constructor(private http: HttpClient) { }

  getEmployees() {
    return this.http.get<any>('http://localhost:8000/api/employee').pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  getOneEmployee(id: number) {
    return this.http.get<any>(`http://localhost:8000/api/employee/${id}`).pipe(
      map((res: any) => {
        return res;
      })
    );
  }


  saveEmployees(data: any) {
    return this.http.post<any>('http://localhost:8000/api/employee', data).pipe(
      map((res: any) => {
        return res;
      })
    );
  }


  editEmployees(data: any, id: any) {
    return this.http.put<any>(`http://localhost:8000/api/employee/${id}`, data).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  deleteEmployees(id: any) {
    return this.http.delete<any>(`http://localhost:8000/api/employee/${id}`).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

}
